const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    body: {
      type: String
    }
  },
  {
    //указывает время создания и редоктирование записи
    timestamps: true
  }
);
// превращает документ в джейсонн
schema.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Post', schema);