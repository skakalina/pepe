const express = require('express');
const bodyParser = require('body-parser');//необходим для того чтобы парсить тело запроса
const path = require('path');
const staticAsset = require('static-asset'); //хэширование статики
const mongoose = require('mongoose'); //подключение к монгусу
const config = require('./config');
const routes = require('./routes');
const session = require('express-session'); //для авторизации. Сессии

const MongoStore = require('connect-mongo')(session);//для хранния сессий в mongoDB

// database
mongoose.Promise = global.Promise;
mongoose.set('debug', config.IS_PRODUCTION);
mongoose.connection
  .on('error', error => console.log(error))
  .on('close', () => console.log('Database connection closed.'))
  .once('open', () => {
    const info = mongoose.connections[0];
    console.log(`Connected to ${info.host}:${info.port}/${info.name}`);
  });
mongoose.connect(config.MONGO_URL, { useMongoClient: true });

const app = express();
// сиссии
app.use(
  session({
    secret: config.SESSION_SECRET,
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);

// sets and uses
app.set('view engine', 'ejs'); //подключение шаблона
app.use(bodyParser.urlencoded({ extended: true }));//парсит тело документа
app.use(bodyParser.json());
app.use(staticAsset(path.join(__dirname, 'public')));  //хэширование статики
app.use(express.static(path.join(__dirname, 'public'))); //подключение статики
app.use(
  '/javascripts',
  express.static(path.join(__dirname, 'node_modules', 'jquery', 'dist')) //подключение  jquery
);
// маршруты
app.get('/', (req, res) => {
  const id = req.session.userId;
  const login = req.session.userLogin;

  res.render('index', {
    user: {
      id,
      login
    }
  });
});
app.use('/api/auth', routes.auth);
app.use('/post', routes.post);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.render('error', {
    message: error.message,
    error: !config.IS_PRODUCTION ? error : {}
  });
});

app.listen(config.PORT, () =>
  console.log(`Example app listening on port ${config.PORT}!`)
);